"""
The first lesson practical solutions.
"""


# Strings

# 1. Declare a string of value “Practical”
x = "Practical"
# 2. Print out “act” by using indexes (string[x:y])
print(x[2:5])
# 3. Print out the string in all uppercase and all lower case
print(x.upper(), x.lower())
# 4. Add the string “ work” to the string you have defined
x += " work"
print(x)
# 5. Replace “al” with “e” in the string
x = x.replace("al", "e")
print(x)

# Working with input
name = input("What’s your name")
# 1.
print("Hi {}! I’m learning python".format(name))
# 2.
print(name[0])

# Numbers
# 1.
x = input("What's the first number?")
y = input("What's the second number?")
print(int(x) + int(y))  # We have to tell python they are integers!
# 2.
numbers = input("What are your two numbers?")
list_of_numbers = numbers.split(" ")  # Break apart by space into a list
x = list_of_numbers[0]  # Get first number
y = list_of_numbers[1]  # Get second number
print(int(x) + int(y))  # We have to tell python they are integers!

# Lists
# 1. Define a list of [“Bart”, “Homer”, “Lisa”]
simpsons = ["Bart", "Homer", "Lisa"]
print(simpsons)
# 2. Add Marge to the list
simpsons.append("Marge")
print(simpsons)
# 3. Delete Homer from the list
simpsons.remove("Homer")
print(simpsons)
# 4. Print the first and last elements in the list
print(simpsons[0], simpsons[-1])
# 5. Check if the list contains Bart
print("Bart" in simpsons)
# 6. Check if the list contains Bart and Maggie
print("Bart" in simpsons and "Maggie" in simpsons)
