"""
Note that the comment at the start of each function is called a doctest (google it!)
They are simple tests for each function.

I've commented out the input (conditional part 2) so that you can run this file and see the
test output.
"""


# Conditionals
# 1.
age = 17
if age < 18:
    print("No Entry")
else:
    print("Welcome")

# 2.
# Note we use .lower() to make the users input lowercase for comparison
# name = input("What's your name?")
# feel = input("Hey {} how you doing?".format(name))
# if feel.lower() == "good":
#     print("That's great!")
# elif feel.lower() == "bad":
#     print("aw no!")
# else:
#     print("kk")



# Functions
# 1. & 2.
# Note that once a function returns it stops running, so we can avoid an "else"
def divide(x, y):
    """
    >>> divide(6, 3)
    2.0
    >>> divide(4, 0)
    Error!
    """
    if y == 0:
        print("Error!")
        return None
    return x / y


# 3.
def biggest(x, y, z):
    """
    >>> biggest(3, 2, 1)
    3
    >>> biggest(1, 3, 2)
    3
    >>> biggest(1, 2, 3)
    3
    """
    if x > y and x > z:
        return x
    elif y > z:
        return y
    else:
        return z


# 4.
# Note that as xx in yy gives us True or False we don't need to use an if statement
def contains(string1, string2):
    """
    >>> contains("Cat", "catdog")
    True
    >>> contains("catdog", "cat")
    False
    >>> contains("dawg", "catdog")
    False
    """
    return string1.lower() in string2.lower()


# 4.
# This one is a little more complex, the basic solution is this which counts words after
# a split, in reality you would probably want to strip out duplicate spaces.
def how_many_words(string):
    """
    >>> how_many_words("Hello World")
    2
    >>> how_many_words("This is me")
    3
    >>> how_many_words("")
    0
    """
    if len(string) == 0:
        return 0
    return len(string.split(' '))


import doctest
doctest.testmod(verbose=True)