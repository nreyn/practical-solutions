# Basic Loops
# 1. Define a list of strings
breakfast = ['Coffee', 'Oats', 'Apple']

# 2. Write a for loop that will print out each string
for food in breakfast:  # Remember that 'food' can be any variable name here!
    print(food)

# 3. Write a while loop that will print out each string
count = 0
while count < len(breakfast):  # len(breakfast) will be 3, but the last index is 2.
    print(breakfast[count])
    count += 1

# Loops, Functions and Condition
# 4. Write a function called highest_number that will return the greatest number in a list
def highest_number(nums):
    highest = None  # We start at None, because the list could be negative numbers too!
    for n in nums:
        if highest is None or n > highest:  # If highest is None, we haven't set it yet - so set it!
            highest = n
    return highest

# Usage example:
print(highest_number([59, 1, 5, 42]))
print(highest_number([59, 1, 86, 42]))

# 5. Write a function called happy_teacher that will print a greeting before each student's name in the list.
def happy_teacher(greeting, students):
    for s in students:
        print("{} {}".format(greeting, s))  # String formatting where {} is replaced in order from the format
        # print(greeting + " " + s) # is also acceptable of course.

happy_teacher('Hello', ['Tom', 'Emma'])
happy_teacher('Hey', ['Lauren', 'Max'])

# Loops and Dictionaries
# 6. Create this data structure (list of dictionaries):
person = [{ 'name': 'Bob' }, { 'name': 'Jack' }, { 'name': 'Sue' }]

# 7. Loop though this printing the person names
for p in person:
    print(p['name'])  # You can also use p.get('name')

# 8. Delete the student with the longest name
longest_name_length = None
student_with_longest = None

for s in person:
    name_length = len(s['name'])  # Store the current students name length
    if longest_name_length is None or name_length > longest_name_length:
        # Same as above, if we dont have a current max value, set it
        # Otherwise if this students name is longer than the stored, save it.
        longest_name_length = name_length
        student_with_longest = s  # Save the actual student/person dict too!

print(student_with_longest)
print(longest_name_length)

if student_with_longest is not None:  # Making sure we have a result before we try and remove it
    person.remove(student_with_longest)  # We can remove from a list by value too!
    print(person)

# 9. Create this structure (list of lists)
matrix = [ [ 2, 3 ], [5, 2] ]
# 10. What is the length of matrix? How would I print 5 from this? i.e. is it matrix[1]?
print(len(matrix))  # It's a list of lists, there's two lists inside the list. (2)
print(matrix[1])  # This will print the list at index 1 which is [5,2]
