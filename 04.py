# Nested Loops
# 1. Create this structure (list of lists)
matrix = [[2, 3], [5, 2]]

# 2. Write a for loop that will print out each number
for outer in matrix:  # Outer will be the outer values, i.e. first loop outer = [2,3] second loop outer = [5,2]
    for inner in outer:  # Inner loops through the outer lists
        print(inner)

# 3. Write a while loop that will print out each number
outer_count = 0
while outer_count < len(matrix):  # Loop through the outer lists with our count
    inner_count = 0               # Make a new count to count the inner loops
    while inner_count < len(matrix[outer_count]):
        # This is the tricky part, remember that square brackets [] accesses a specific index, so we are getting
        # the length of matrix[0], matrix[0] is [2,3]
        value = matrix[outer_count][inner_count]
        # This is how we access the value, matrix[0] is [2,3] therefore matrix[0][0] is 2, and matrix[0][1] is 3
        print(value)
        inner_count += 1
    outer_count += 1

# Functions in Loops
# 4. Write a function called average that will return the average of a list

def average(nums):
    """
    >>> average([59, 1, 5, 42])
    26.75
    >>> average([]) == None
    True
    >>> average([1])
    1.0
    """
    if len(nums) == 0:
        return None
    total = 0
    for number in nums:
        total += number
    return total/len(nums)

# 5. Use this function to find the average of each list inside the matrix defined above
# 6. Use this function to then find the average of all of the lists
results = []
for m in matrix:            # Loop through our matrix
    result = average(m)     # Get the average of this list in the matrix i.e. [2,3]
    results.append(result)  # Add the resulting average to a list of results
print(average(results))     # Perform an average on the list of results

# Functions with More Complex Data Structures
# 7. (Advanced) Define the following structure (a list of dictionaries, with lists inside them)

classes = [{
    'name': 'Finance',
    'students': ['Bob', 'Jack', 'Lauren']
}, {
    'name': 'Astro',
    'students': ['Foo', 'Lauren']
}]

# 8. Write a function that will take a name, and output which classes they are in, returning them in a list.
# Remembering that there could be more than two classes.

def what_classes(classes, name):
    """
    >>> what_classes(classes, 'Bob')
    ['Finance']

    >>> what_classes(classes, 'Lauren')
    ['Finance', 'Astro']
    """
    # Logic: loop through each class, check if the student is in that class
    # if they are in that class store the class name
    results = []
    for cls in classes:                 # Loop through the classes
        if name in cls['students']:     # See if name is in the list of students for this class
            results.append(cls['name']) # Store the class name in our list of results
                                        # Otherwise do nothing!
    return results                      # Return our list of classes


# 9. Write a function that will find students who are in both classes, returning them in a list.
# What if there are more than two classes?

def both_classes(classes):
    """
    >>> both_classes(classes)
    ['Lauren']
    """
    # Logic: Get the students from the first class (common_students), as we loop through, check to see if they
    # exist in the next classes student list.
    common_students = None  # Start with None students (think about why we can't start with an empty list)
    for cls in classes:     # Loop over each class
        this_classes_students = list(cls['students'])   # Take a copy of the students from the current class using list()
                                                        # We do this so that we aren't deleting things from the original list
        if common_students is None:                     # If common students is none then we are checking the first class
            common_students = this_classes_students     # So all the students are common students currently

        for student in list(common_students):           # Now we want to check each student in common students
            if student not in this_classes_students:    # To see if they dont exist inside this classes student list
                common_students.remove(student)         # If they don't exist then we need to remove them
                                                        # Note: we copy the list again as you shouldn't delete things from the same
                                                        # List you are looping over
    return common_students





# Just running our tests
import doctest
doctest.testmod()