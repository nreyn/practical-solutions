# 1. Define the class below

class Animal:
    name = None

    def __init__(self, name):
        self.name = name

    def noise(self):
        print("Moo")

# 2. Create an instance of the animal class and print its name

me = Animal("Bob")
print(me.name)

# 3. Add a new variable called age to the constructor and class

class Animal:
    name = None
    age = 0

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def noise(self):
        print("Moo")

class Dog(Animal):  # We created a new subclass Dog of type Animal
    pass  # pass is a filler word, nothing here for now

bob = Dog("Bob", 0)
print(bob.age)  # Prints 0
doug = Dog("Doug", 12)
print(doug.age)  # Prints 12

# 4. Add a new function called “rename” which will rename the animal

class Animal:
    name = None
    age = 0

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def noise(self):
        print("Moo")

    def rename(self, new_name):
        self.name = new_name

class Dog(Animal):  # We created a new subclass Dog of type Animal
    pass  # pass is a filler word, nothing here for now

bob = Dog("Bob", 0)
print(bob.name) # Prints Bob
bob.rename("Doug")
print(bob.name) # Prints Doug

# 5. Given the above definition for Cat, write a Dog class that has a noise function that prints “woof”

class Dog(Animal):
    def noise(self):
        print("woof")

bob = Dog("Bob", 0)
bob.noise()  # Prints woof

# 6. Add a new class variable called “walks”, to count the number of walks the dog has been on
# 7. Add a new class function called “Walk” which will increment this number

class Dog(Animal):
    walks = 0
    def walk(self):
        self.walks += 1

bob = Dog("Bob", 0)
print(bob.walks)  # Prints 0
bob.walk()
print(bob.walks)  # Prints 1
