
import csv  # Importing the CSV library of code for use in this file.

"""
1. Write code that will generate the following file, saved as q1.txt

File
Output
In
Python
"""

file = open("q1.txt", "w")  # Opening our file in write mode
file.write("File\nOutput\nIn\nPython")  # Write out the content, with \n meaning new line
file.close()  # Close the file (we are done with it!)

"""
2. Write code that will generate the following CSV file, as q2.csv

Item, Quantity
Apple, 3
Bacon, 2
Orange, 4
Onion, 1
"""

with open('q2.csv', 'w') as file:  # We are using a 'context handler' now, this will automatically close the file
    writer = csv.writer(file, lineterminator='\n')  # Make a new CSV writer for the file
    writer.writerow(['Item', 'Quantity'])
    writer.writerow(['Apple', 3])
    writer.writerow(['Bacon', 2])
    writer.writerow(['Orange', 4])
    writer.writerow(['Onion', 1])

"""
3. Write code that will read the q2.csv file line by line, and write it to a new file called q3.csv
"""

with open('q2.csv', 'r') as read_file:
    reader = csv.reader(read_file)
    with open('q3.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for row in reader:
            writer.writerow(row)

"""
4. Copy the previous code and modify it such that the new file will contain a ‘total’
row at the bottom of the file, calculated by adding up all of the quantities, save as q4.csv.
Make sure you are actually calculating the total!
"""
# using csv reader
with open('q3.csv', 'r') as read_file:
    reader = csv.reader(read_file)
    with open('q4a.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        total = 0
        for row in reader:
            if row[1] != 'Quantity' and row != []: # Just incase there's an empty row
                quantity = int(row[1])
                total += quantity
            writer.writerow(row)
        writer.writerow(['Total', total])

# Using dict reader and dict writer

with open('q3.csv', 'r') as read_file:
    reader = csv.DictReader(read_file)
    with open('q4b.csv', 'w') as write_file:
        fields = ['Item', 'Quantity']
        writer = csv.DictWriter(write_file, fieldnames=fields) # The dict reader doesn't know which fields to use
        writer.writeheader()  # Write out the header row (item, quantity)
        total = 0
        for row in reader:
            quantity = row['Quantity']
            total += int(quantity)
            writer.writerow(row)
        writer.writerow({ 'Item': 'Total', 'Quantity': total })  # Make a new dict for the total and write it out

"""
4. Copy paste the above and modify it so that it will classify the items and add it as a new column. The classification rules are:
○ An item is fruit if it is an apple or orange
○ An item is meat if it is bacon
○ Otherwise it is “other”
Save this as q5.csv
"""
with open('q4b.csv', 'r') as read_file:
    reader = csv.DictReader(read_file)
    with open('q5.csv', 'w') as write_file:
        fields = ['Item', 'Category', 'Quantity']  # We've added a new category column!
        writer = csv.DictWriter(write_file, fieldnames=fields) # The dict reader doesn't know which fields to use
        writer.writeheader()
        for row in reader:
            if row['Item'] != 'Total': # We don't need to add a category to the total row!
                fruits = ['Apple', 'Orange']  # Out list of different fruits
                if row['Item'] in fruits:  # If the item is in our list of fruits it's a fruit
                    row['Category'] = 'Fruit'  # Add a new category property to the dictionary!
                elif row['Item'] == 'Bacon':
                    row['Category'] = 'Meat'
                else:
                    row['Category'] = 'Other'
            writer.writerow(row)

"""
6. Copy paste the above and modify it so that it will add totals for each category. Save this as q6.csv.

This is pretty advanced!
"""

with open('q5.csv', 'r') as read_file:
    reader = csv.DictReader(read_file)
    cat_totals = {}  # We have made an empty dictionary to store our subtotals
    original_values = list(reader)  # This stops us needing to re open the file if we want to read it twice.
    for row in original_values:
        category = row['Category']  # Get the category for this row
        if category != '':  # We only care if it has a category (i.e. not the total row)
            if category not in cat_totals:  # If it's not in our dictionary yet
                cat_totals[category] = 0  # Add it to our dictionary with a zero e.g. { 'Fruit': 0 }
            cat_totals[category] += 1  # Add one to the count for this category e.g. { 'Fruit' : 1 }

    with open('q6.csv', 'w') as write_file:
        fields = ['Item', 'Category', 'Cat Total', 'Quantity']  # We've added a new sub category column!
        writer = csv.DictWriter(write_file, fieldnames=fields)
        writer.writeheader()
        for row in original_values: # We can now loop through, and write out each line with its subtotal
            category = row['Category']  # Get the category for this row
            if category in cat_totals:  # If we have a total stored for this category
                row['Cat Total'] = cat_totals[category]  # Get the cat totals for this category
            writer.writerow(row)
