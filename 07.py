import doctest
import unittest

def divide(x, y):
    """
    Divide two numbers
    >>> divide(2, 1)
    2.0
    >>> divide(3, 4)
    0.75
    >>> divide(3, 0) # Doctests match nothing with None, strange right! They are very simplistic tests.

    """
    if y == 0:  # Obviously we can't divide by zero, so we are going to check for this and return None
        return None
    return x/y  # We don't need an else here, as if the y==0 condition happened the function has already stopped and returned!


class TestDivide(unittest.TestCase):
    def test_divide(self):
        self.assertEqual(divide(100, 25), divide(4, 1))
        self.assertTrue(divide(3, 4) == 0.75)
        self.assertFalse(divide(1, 0) == 1)  # We expect divide by zero to return None, so this should be false and pass!


# Comment out which test you dont want to run (can't have both!)
unittest.main(verbosity=2)
# doctest.testmod(verbose=True)
