import re
s = """
In 2009, on the 1st the price was $30.00, on the 20th it was $94.12.
"""
results = re.findall( r'your regex here' , s)

# 1. Write the regex to extract the year (imagine it could be any year).
years = re.findall(r'\d{4}', s)  # Pattern says \d (digits) {4} (4 of them)
print('Years:', years)

"""
2. Write the regular expression to extract the dollar figures
i.e. your results should be [ '$30.00' ,  '$94.12' ]
"""
prices = re.findall(r'\$\d+\.\d{2}', s)
print('Dollars:', prices)
"""
The above pattern says $ followed by one or more digits (\d+), followed by a dot, and two digits.
Note we had to escape the dollar sign and the dot as they are special regex characters


3. Write the regular expression to extract the dates
I.e. your results should be [ '1st' ,  '20th']
"""

dates = re.findall(r'(\d{1,2}(?:st|nd|rd|th))', s)
print('Dates:', dates)

"""
This pattern says one or two digits (\d{1,2}), followed by st or nd or rd or th
The tricky part here is what's called capture groups (i.e. the results you get) which is in the brackets ()
This (\d{1,2}(st|nd|rd|th)) would give us a result of [('1st', 'st'), ('20th', 'th')], the first result being the first set of ()
The second result being the st/nd/rd/th.

We can remove something from our capture group by putting ?: after the first ( as I have done.


4. Using a combination of regex and other logic, produce an dictionary like the
following:
{
  'Year' :  2009 , #  This is a number
  'Date' :  '1st' ,
  'Price' :  30.0  # This is a float
 }
"""
result = {
 'Year': int(years[0]),
 'Date': dates[0],
 'Price': float(prices[0].replace('$', ''))  # We replace the dollar sign with nothing, then convert to float
}
print(result)

# We could also do this in a loop!
count = 0
results = []
while count < len(dates):
    results.append({
        'Year': int(years[0]),
        'Date': dates[count],
        'Price': float(prices[count].replace('$', ''))  # We replace the dollar sign with nothing, then convert to float
    })
    count += 1
print(results)


"""
5. Write (and test!) a regular expression that will match:
001111001
001001
But not match
11
001100111

The easiest way to do this is just look at the patterns, the first two start with 00 and end
with 01. So a simple pattern would be 01[01]*01.
This says the string must start with 01, end with 01 and have any number of 1's and 0's in between.

"""
