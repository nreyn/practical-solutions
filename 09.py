import csv
# Basics
# 1. Write a loop that will add up all the even numbers from 1 to 100 inclusive. Your result should be 2550.
sum = 0
for n in range(0, 101):
    if n % 2 == 0:
        sum += n
print('1.', sum)

# or

sum = 0
for n in range(0, 101, 2):  # This says skip every second
    sum += n
print('1.', sum)


# 2. Write a function that will take two strings as input, and return the longest string,
# if they are the same length it will return the first one.

def longest(s1, s2):
    """
    9. Doctests
    >>> longest("Cat", "Rabbit")
    Cat
    >>> longest("Rabbit", "Cat")
    Rabbit
    >>> longest("Dog", "Cat")
    Dog
    """
    if len(s2) > len(s1):
        return s2
    return s1

print(longest("Cat", "Rabbit"))
print(longest("Rabbit", "Cat"))
print(longest("Cat", "Dog"))

"""
File Input and Output
3. Write code that will write the following to File1.csv.

Student, Mark
Foo, 10
Bar, 50
Alan, 20
Tim, 30
"""

with open("File1.csv", 'w+') as f:
    writer = csv.writer(f)
    writer.writerow(['Student', 'Mark'])
    writer.writerow(['Foo', '10'])
    writer.writerow(['Bar', '50'])
    writer.writerow(['Alan', '20'])
    writer.writerow(['Tim', '30'])

"""
4. Write code that will calculate the average of the marks column, the minimum mark and the maximum mark and add it to a new file along with the rest of the content.
I suggest you do this in steps, figure out the average then the rest.
"""
with open('File1.csv', 'r') as f:
    with open('File2.csv', 'w+') as w:
        reader = csv.DictReader(f)
        writer = csv.DictWriter(w, fieldnames=['Student', 'Mark'])
        writer.writeheader()
        count = 0
        total = 0
        highest = None
        lowest = None
        for row in reader:
            current_mark = row['Mark']
            total += int(current_mark)  # Add their mark to the total
            if highest is None or current_mark > highest:  # see if the highest is bigger than our current highest
                highest = current_mark
            if lowest is None or current_mark < lowest:  # see if the lowest is lower than our current lowest
                lowest = current_mark
            count += 1  # Add one to our count
            writer.writerow(row)  # Write out this row (i.e. copy it to the new file)
        average = total / count
        writer.writerow({  # Write out the calculations!
            'Student': 'Average',
            'Mark': average
        })
        writer.writerow({
            'Student': 'Max',
            'Mark': highest
        })
        writer.writerow({
            'Student': 'Min',
            'Mark': lowest
        })

"""
Classes
5. Create a new class called SimpleShopping, the class will need a name and will also have a dictionary as a property. As below:
"""
class SimpleShopping:
    def __init__(self, name):
        self.name = name
        self.items = dict()

# 6. Add two new methods to the shopping class “add” and “remove”, add will add a new item to the shopping list, while remove will remove it.

    def add(self, item):
        self.items[item] = 1

    def remove(self, item):
        if item in self.items:   # Only delete it if it exists
            del self.items[item]

shop = SimpleShopping("New")	# shop.items: {}
print('Q7', shop.items)
shop.add("Apple")  	# shop.items: {'Apple': 1}
print('Q7', shop.items)
shop.add("Banana")	# shop.items: {'Apple': 1, 'Banana': 1}
print('Q7', shop.items)
shop.add("Apple")		# shop.items: {'Apple': 1, 'Banana': 1}
print('Q7', shop.items)
shop.remove("Apple")  	# shop.items: {'Banana': 1}
print('Q7', shop.items)

"""
7. Currently our shopping list assumes we only ever have or need one of everything.
Create a subclass of our SimpleShopping class called SmartShopping that will keep track of quantities properly.
"""

class SmartShopping(SimpleShopping):
    def add(self, item):
        if item not in self.items:  # Add it if it isn't there already
            self.items[item] = 0    # Start at zero
        self.items[item] += 1       # Increment!

    def remove(self, item):
        if item in self.items:        # If it exists
            self.items[item] -= 1     # Subtract one
            if self.items[item] == 0: # We will delete it from the dictionary if it has no quantity
                del self.items[item]

shop = SmartShopping("New")	# shop.items: {}
print('Q8', shop.items)
shop.add("Apple")  	# shop.items: {'Apple': 1}
print('Q8', shop.items)
shop.add("Banana")	# shop.items: {'Apple': 1, 'Banana': 1}
print('Q8', shop.items)
shop.add("Apple")		# shop.items: {'Apple': 2, 'Banana': 1}
print('Q8', shop.items)
shop.remove("Apple")  	# shop.items: {'Apple': 1, 'Banana': 1}
print('Q8', shop.items)
shop.remove("Apple")  	# shop.items: {'Banana': 1}
print('Q8', shop.items)
"""
Testing
8. Write appropriate doc tests for the function you wrote in Q2.
See above
"""