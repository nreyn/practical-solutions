

class SimpleShopping:
    def __init__(self, name):
        self.name = name
        self.items = dict()

    def add(self, item):
        self.items[item] = 1

    def remove(self, item):
        if item in self.items:
            del self.items[item]

c = SimpleShopping("New")
print(c.items)
c.add('Orange')
c.add('Orange')
c.add('Aldo')
print(c.items)
c.remove('Banana')
print(c.items)